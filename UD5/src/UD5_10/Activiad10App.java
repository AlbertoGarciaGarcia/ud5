package UD5_10;
import javax.swing.JOptionPane;

public class Activiad10App {

	public static void main(String[] args) {
		double totalVentas = 0;
		String numeroVentas = JOptionPane.showInputDialog(null,"Introduce la cantidad de ventas realizadas");
		double numeroVentasDouble = Double.parseDouble(numeroVentas);
		for (int i = 0; i < numeroVentasDouble; i++) { // Iteramos tantas veces como ventas hayan
			String venta = JOptionPane.showInputDialog(null,"Introduce el precio de la venta " + i);
			double doubleVenta = Double.parseDouble(venta);  
			totalVentas = totalVentas + doubleVenta; // Hacemos un contador para tener toda la cantidad de suma
			
		}
		JOptionPane.showMessageDialog(null,"El total de ventas es  " + totalVentas);
		
	}

}
