package UD5_13;
import javax.swing.JOptionPane;

public class CalculadoraInversa {

	public static void main(String[] args) {
		String numeroUno = JOptionPane.showInputDialog(null,"Introduce el primer numero");
		String numeroDos = JOptionPane.showInputDialog(null,"Introduce el segundo numero");
		int unoInt = Integer.parseInt(numeroUno);
		int dosInt = Integer.parseInt(numeroDos);
		String signo = JOptionPane.showInputDialog(null,"Introduce el signo aritmetico ");
		
		// Haremos un if para cada simbolo aritmetico y trabajaremos entorno a ello
		if(signo.equals("+")) {
			JOptionPane.showMessageDialog(null,"La operacion en caso de suma es " + (unoInt+dosInt));
		}
		else if(signo.equals("-")) {
			JOptionPane.showMessageDialog(null,"La operacion en caso de resta es " + (unoInt - dosInt));
		}
		else if(signo.equals("*")) {
			JOptionPane.showMessageDialog(null,"La operacion en caso de multiplicacion es " + (unoInt * dosInt));
		}
		else if(signo.equals("/")) { // Pasaremos a double las variables para que de un resultado double 
			double doubleUno = Double.parseDouble(numeroUno);  
			double doubleDos = Double.parseDouble(numeroDos);  
			double resultadoDivision = doubleUno / doubleDos;
			JOptionPane.showMessageDialog(null,"La operacion en caso de division es " + resultadoDivision);
		}
		else if(signo.equals("^")) {
			JOptionPane.showMessageDialog(null,"La operacion en caso de potencia  es " + Math.pow(unoInt, dosInt)); // Utilizaremos la funcion pow 
		}
		else if(signo.equals("%")) {
			JOptionPane.showMessageDialog(null,"La operacion en caso de residuo  es " + unoInt % dosInt );
		}
	}
	

}
