package UD5_6;
import javax.swing.JOptionPane;

public class Actividad6App {
	static final int IVA = 21; // Declaramos como variable final
	public static void main(String[] args) {
		
		String precio = JOptionPane.showInputDialog(null,"Introduce el precio del producto");
		double precioDouble = Double.parseDouble(precio);  
		double cantidadIVA = (precioDouble/100) * IVA;
		double precioFinal = precioDouble + cantidadIVA;
		
		JOptionPane.showMessageDialog(null,"El precio con el IVA es " + precioFinal);
		
	}

}
