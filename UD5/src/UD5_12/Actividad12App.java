package UD5_12;
import javax.swing.JOptionPane;

public class Actividad12App {

	public static void main(String[] args) {
		String contraseņa = "abcd12345";
		String intentoContraseņa = " ";
		int contador = 0;
		
		do {
			 intentoContraseņa = JOptionPane.showInputDialog(null,"Introduce la contraseņa");	
			contador++; // Por cada error sumaremos un intento
			System.out.println(contador);
		}
		while( !contraseņa.equals(intentoContraseņa) &&  contador <= 2 ); // Haremos como condicion que el contador no pase de los 3 intentos y que la contraseņa no sea igual
		if(contraseņa.contentEquals(intentoContraseņa)) {
			JOptionPane.showMessageDialog(null,"Enhorabuena!!!");
		}
		
	}

}
