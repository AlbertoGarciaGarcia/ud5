package UD5_3;
import java.util.Scanner;

public class Actividad3App {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in); 	// Importamos el scanner
		
		System.out.println("Cual es tu nombre?");
		String nombre = teclado.next(); // Hacemos que sea el valor de la variable la siguiente linea que entre
		System.out.println("Bienvenido " + nombre);
		
		teclado.close();
		
	}

}
